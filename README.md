## Style Guidelines

A thought-out style guide for the English language, Markdown, and other things. Made for programmers, by a programmer.

### Motivation

I've come to notice that most amateur repositories use broken English in one way or another, or have a very inconsistent style. Therefore, I created these guidelines, in order to make these projects better than ever! As this project evolves, it will make it easier than ever to make a wiki, improve your README, or make your announcements clear as crystal.

### License
This project is distributed under the `CC BY-SA 4.0` license. Check the `LICENSE` file for more information.

<a href="https://creativecommons.org/licenses/by-sa/4.0">
  <img src="https://codeberg.org/thatonecoder/assets/raw/branch/main/images/cc-by-sa.svg" alt="CC BY-SA" width="88" height="31">
</a>