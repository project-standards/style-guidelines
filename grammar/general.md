## Introduction
Speaking in English is a rather difficult job for some. This part of the guide gives some opinionated guidelines for you to follow, making the speech more robust. Since it may be difficult to navigate through the entire file, I have decided to provide a not quite useful Table of Contents below.

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#be-creative">Don't think, just write</a>
      <ul>
        <li><a href="#widen-your-vocabulary">Widen your vocabulary</a></li>
        <li><a href="#revise">Revise, revise, revise!</a></li>
        <li><a href="#adapt-your-writing-style">Adapt your writing style</a></li>
      </ul>
    </li>
    <li>
      <a href="#how-to-use-like">How to use the word “like”</a>
      <ul>
        <li><a href="#avoid-using-as-filler">Avoid using it as a filler</a></li>
        <li><a href="#like-replacements">Replacements</a></li>
      </ul>
      <ul>
    </li>
  </ol>
</details>

### Don't think, just write {#be-creative}
Since most tend to forget, it is paramount to write everything down, without thinking too much initially — don't worry, you can double-check later. This allows you to have more elaborate writing, as opposed to if you tried to make your writing perfect right away.

#### Widen your vocabulary {#widen-your-vocabulary}
This is the cold truth; most people have a very stale vocabulary, making them seem like idiots, regardless of them being so. Therefore, it's needed for you to continuously train your skills, in order to pass off as a good writer.

#### Revise, revise, revise! {#revise}
Most people post without double-checking — but why is this harmful? Well, it's actually quite simple! When you are writing, you are mostly putting all of your thoughts on paper (or, in this case, a screen), your brain is focused on grasping the concept in a way that is easy for you; however, it doesn't focus on grammar too much, making it essential to revise later on. Revising is the key to making your writing perfect — I myself have revised this text multiple times, in order to make it legible. I'd also recommend for you to use [Grammark](https://grammark.org) to check the text for wordiness, and [LanguageTool](https://languagetool.org) to check for other grammatical errors; so far, these are the best tools I've found.

#### Adapt your writing style {#adapt-your-writing-style}
In all documentation, there are easier and harder parts. Knowledge increases throughout reading documentation, and vocabulary is no exception. Given this, you should simplify the basics, and increase in complexity gradually, in just the same amount that your readers will increase their knowledge. Additionally, you should add a glossary, as some terms are very niche.

### How to use the word “like” {#how-to-use-like}
The word “like” can be useful in certain contexts; but if overused, can make you sound less intelligent, *even* if you are an expert at a theme. In this section, you'll find replacements that improve your language.

#### Avoid using it as a filler {#avoid-using-as-filler}
OK, so, like, most people like, use it in this way like, a lot, right?

*Phew*, you *really* ought not to use it like that. That's the largest usage of the word “like”, and by far the worst offender. Many people use it in their writing, similar to “you know”, “right”, etc — but you could simply remove all the “likes”, and make the sentence somewhat readable.

#### Replacements {#like-replacements}

| With “like” | Replacement |
| ----- | ----- |
| I was(,) like (saying) | I said / yelled / whispered (depends on context) |
