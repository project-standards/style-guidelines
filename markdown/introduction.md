## Introduction
Markdown files are basically normal text files. The file extension `.md` specifies that a file can be rendered as Markdown.
You can also use Markdown in many parts of Codeberg (Issues, Pull Requests, etc.).

### Text section

To write a Markdown file, simply create a new file, and edit it with a text editor of your choice.
Markdown doesn't consider single line breaks as the start of a new paragraph.
You can write all your text into one long line or introduce a new line every once in a while.
It is common practice to introduce a new line at around 80 characters to enable users to easily read the plain un-rendered version of the Markdown file.
However, it's recommended to make a line break in Markdown when it makes sense, e.g. at the end of a sentence.
It makes diffs easier to understand, as the context of the complete sentence is preserved.

If you want to start a new paragraph, use two or more empty new lines to separate the text.
Beware that when rendering with Forgejo, line breaks are rendered differently in repos and comment fields.
For example, one line break in a comment leads to a new paragraph.

#### Highlighting text sections

In paragraphs, it is possible to highlight passages using **bold** and *italics*.

#### Bold

To make text bold, use two asterisks at the start of the section you want to highlight `**`
At the end of the section, add another two asterisks `**`.

Here is an example.

```
This is **bold text**.
```

This gets rendered as

This is **bold text**.

#### Italics

To make text italic use one asterisk at the start of the section you want to highlight `*`
At the end of the section, add another asterisk `*`.

Here is an example.

```
This is *italic text*.
```

This gets rendered as:

This is *italic text*.

#### Links
Links are used to refer to other articles, sections in articles or other websites.

##### Links with description
It is always good for readability to not only paste a URL into your text but to provide a description of your link. Only the description of the link will be in the rendered form of your text and the link will be added as an HTML hyperlink.

Hyperlinks have the following markup: `[Link description](link-url)`.

For example:

`[Link to Codeberg](https://codeberg.org)`
Gets rendered as:

[Link to Codeberg](https://codeberg.org)

Links without description
To add a link using the URL within your text use < and > to mark the link. For example, if you want to add a link to `https://codeberg.org`, add `<https://codeberg.org>` to your text. This will render as <https://codeberg.org>. You can also just add the link to your text normally to have the same effect: `https://codeberg.org`. However it is easier to navigate to links if they are explicitly marked by the less than < and greater than > characters.

### Forgejo-specific formatting

#### Emoticons
Text may contain references to emoticons which are rendered as a small image, similar to an emoji.
You can render these by typing the name of the emoticon you want to use, surrounded by colons (`:`), like this `:codeberg:`.

#### Referencing issues and pull requests

Issues and pull requests in Codeberg/Forgejo can be referenced in the comments of an issue or a pull request by using a hash `#` followed by the number of the issue or pull request.
The renderer will then include a link to the referenced issue into the comment.
After that, a link to the comment containing the reference will be added to the issues referenced in this way.

#### Checkboxes

You can add checkboxes to comments by using a space surrounded by square brackets `[ ]`. These can be checked/unchecked later, without editing the comment.
This can be useful when creating a roadmap.